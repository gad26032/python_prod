# ---- Пример настройки логгирования для приложения, используя logging ----

# * Logging Cookbook: https://docs.python.org/3/howto/logging-cookbook.html

# logging - стандартный модуль для организации логгирования
import logging
from functools import wraps
# DEBUG	Detailed information, typically of interest only when diagnosing problems.
# INFO	Confirmation that things are working as expected.
# WARNING	An indication that something unexpected happened, or indicative of some problem in the near future (e.g. ‘disk space low’). The software is still working as expected.
# ERROR	Due to a more serious problem, the software has not been able to perform some function.
# CRITICAL	A serious error, indicating that the program itself may be unable to continue running.

# Быстрая настройка логгирования может быть выполнена так:
# logging.basicConfig(filename="gui.log",
#     format="%(levelname)-10s %(asctime)s %(message)s",
#     level = logging.INFO
# )

LOGGING_LEVEL = logging.WARNING
# print(logging.info)
# Можно выполнить более расширенную настройку логгирования.
# Создаём объект-логгер с именем db_admin_gui:
logger = logging.getLogger('app.main')
# logger = logging.getLogger('app.main1')

# Создаём объект форматирования:
formatter = logging.Formatter("%(levelname)s - %(asctime)s - %(pathname)s - %(module)s - %(funcName)s - %(message)s ")

# Возможные настройки для форматирования:
# -----------------------------------------------------------------------------
# | Формат         | Описание
# -----------------------------------------------------------------------------
# | %(name)s       | Имя регистратора.
# | %(levelno)s    | Числовой уровень важности.
# | %(levelname)s  | Символическое имя уровня важности.
# | %(pathname)s   | Путь к исходному файлу, откуда была выполнена запись в журнал.
# | %(filename)s   | Имя исходного файла, откуда была выполнена запись в журнал.
# | %(funcName)s   | Имя функции, выполнившей запись в журнал.
# | %(module)s     | Имя модуля, откуда была выполнена запись в журнал.
# | %(lineno)d     | Номер строки, откуда была выполнена запись в журнал.
# | %(created)f    | Время, когда была выполнена запись в журнал. Значением
# |                | должно быть число, такое как возвращаемое функцией time.time().
# | %(asctime)s    | Время в формате ASCII, когда была выполнена запись в журнал.
# | %(msecs)s      | Миллисекунда, когда была выполнена запись в журнал.
# | %(thread)d     | Числовой идентификатор потока выполнения.
# | %(threadName)s | Имя потока выполнения.
# | %(process)d    | Числовой идентификатор процесса.
# | %(message)s    | Текст журналируемого сообщения (определяется пользователем).
# -----------------------------------------------------------------------------

# Создаём файловый обработчик логгирования (можно задать кодировку):
fh = logging.FileHandler("app.main.log", encoding='utf-8')
fh.setLevel(LOGGING_LEVEL)
fh.setFormatter(formatter)

# Добавляем в логгер новый обработчик событий и устанавливаем уровень логгирования
logger.addHandler(fh)
logger.setLevel(LOGGING_LEVEL)


# Возможные уровни логгирования:
# -----------------------------------------------------------------------------
# | Уровень важности | Использование
# -----------------------------------------------------------------------------
# | CRITICAL         | log.critical(fmt [, *args [, exc_info [, extra]]])
# | ERROR            | log.error(fmt [, *args [, exc_info [, extra]]])
# | WARNING          | log.warning(fmt [, *args [, exc_info [, extra]]])
# | INFO             | log.info(fmt [, *args [, exc_info [, extra]]])
# | DEBUG            | log.debug(fmt [, *args [, exc_info [, extra]]])
# -----------------------------------------------------------------------------


def log_debug(msg):
    def decorator(func):
        @wraps(func)
        def decorated(*args, **kwargs):
            print(func.__module__)
            print(func.__name__)
            print(func.__code__.co_filename)
            logger.debug(msg)
            res = func(*args, **kwargs)
            return res
        return decorated
    return decorator


def log_info(msg):
    def decorator(func):
        @wraps(func)
        def decorated(*args, **kwargs):
            print(func.__module__)
            print(func.__name__)
            print(func.__code__.co_filename)
            logger.info(msg)
            res = func(*args, **kwargs)
            return res
        return decorated
    return decorator


def log_warning(func, msg):
    logger.warning(msg)
    return func()


def log_error(func, msg):
    logger.error(msg)
    return func()


def log_critical(func, msg):
    logger.critical(msg)
    return func()


if __name__ == '__main__':
    # Создаём потоковый обработчик логгирования (по умолчанию sys.stderr):
    console = logging.StreamHandler()
    console.setLevel(LOGGING_LEVEL)
    console.setFormatter(formatter)
    logger.addHandler(console)
    # В логгирование передаем имя текущей функции и имя вызвавшей функции
    logger.info('Тестовый запуск логгирования')
