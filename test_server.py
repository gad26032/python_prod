from socket import *
import time
import json
from multiprocessing import Process, Queue, Manager
from multiprocessing.managers import SyncManager, ListProxy
from multiprocessing.queues import Queue
import sys
import getopt
import select
import pytest
from server import create_pipes, connection_handler, start_server, msg_compress, client_handler, msg_decompress, \
    client_qualifier, \
    ADDR, PORT, server_info


def test_client_qualifier():
    def client():
        while True:
            try:
                s = socket(AF_INET, SOCK_STREAM)
                s.connect((ADDR, PORT))
                s.close()
                break
            except Exception as e:
                print(e)
            time.sleep(1)

    def server(msg):
        server_info(PORT, ADDR)
        sok = socket(AF_INET, SOCK_STREAM)
        sok.setsockopt(SOL_SOCKET, SO_REUSEPORT, 1)
        sok.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        sok.bind((ADDR, PORT))
        sok.listen(5)
        client, addr = sok.accept()
        manager, clients_senders_q, clients_receivers_q, msgs_q = create_pipes()
        client_qualifier(clients_receivers_q, clients_senders_q, manager, client, msg)
        sok.close()
        return clients_senders_q, clients_receivers_q

    presence_msg_send = {
        "action": "presence",
        "time": '',
        "type": 'send',
        "user": {
            "account_name": "C0deMaver1ck",
            "status": "Yep, I am here!"
        }
    }

    presence_msg_receive = {
        "action": "presence",
        "time": '',
        "type": 'receive',
        "user": {
            "account_name": "C0deMaver1ck",
            "status": "Yep, I am here!"
        }
    }

    client_p = Process(target=client)
    client_p.start()
    clients_senders_q, clients_receivers_q = server(presence_msg_send)
    assert len(clients_senders_q) == 1
    client_p.terminate()

    time.sleep(1)
    client_p = Process(target=client)
    client_p.start()
    clients_senders_q, clients_receivers_q = server(presence_msg_receive)
    assert len(clients_receivers_q) == 1
    client_p.terminate()

    print('Распределение по очередям работает нормально')


def test_create_pipes():
    mgr, senders_q, receivers_q, m_q = create_pipes()

    assert isinstance(mgr, SyncManager)
    assert isinstance(senders_q, ListProxy)
    assert isinstance(receivers_q, ListProxy)
    assert isinstance(m_q, Queue)
    print('сreate_pipes работают нормально')


def test_start_server():
    print('Проверка старта сервера')

    sok = start_server(ADDR, PORT)
    addr, port = sok.getsockname()
    assert isinstance(sok, socket)
    assert addr == ADDR
    assert port == PORT
    print('Сервер стартует нормально')


def test_msg_compress():
    t_string = 'Тестовая строка'
    t_string_byte = json.dumps(t_string).encode('utf-8')
    output = msg_compress(t_string)
    assert isinstance(output, bytes)
    assert t_string_byte == output


def test_message_decompress():
    t_string = 'Тестовая строка'
    t_string_byte = json.dumps(t_string).encode('utf-8')
    output = msg_decompress(t_string_byte)
    assert isinstance(output, str)
    assert t_string == output
