# coding: utf-8
# -*- coding: utf-8 -*-
from socket import *
import json
import time
from multiprocessing import Process
import sys
import getopt


def send_msg(msg, conn):

    conn.send(json.dumps(msg).encode('utf-8'))


def msg_parser(byte_msg):
    msg_l = byte_msg.decode('utf-8').split('""')
    if len(msg_l) > 1:
        msg_l[0] += '"'
        msg_l[-1] = '"' + msg_l[-1]
    if len(msg_l) > 2:
        for i in range(1, len(msg_l) - 1):
            msg_l[i] = '"' + msg_l[i] + '"'
    return msg_l


def msg_receiver(conn):
    print(conn)
    while True:
        try:
            msg = conn.recv(16000)
            if msg:
                for m in msg_parser(msg):
                    p = json.loads(m)
                    print('message: ' + p)
        except Exception as e:
            print(e)
        time.sleep(1)


def msg_sender():
    sys.stdin = open("/dev/tty")
    while True:
        try:
            msg = input('enter msg: ')
            if msg != 'exit!':
                sock = socket(AF_INET, SOCK_STREAM)
                sock.connect((ADDR, PORT))
                send_msg(msg,  sock)
            else:

                print('goodbye')
                break
        except Exception as e:
            print(e)
        finally:
            try:
                sock.close()
            except Exception as e:
                print(e)


def client_info(addr, port):
    print('Попытка подключения к серверу \n'
          'ip_addr: {}\n'
          'port: {}\n'.format(addr, port))


if __name__ == '__main__':
    PORT = 7777
    ADDR = 'localhost'

    options, remainder = getopt.getopt(sys.argv[1:], 'a:p:', ['addr=', 'port='])
    for opt, arg in options:
        print(opt)
        if opt in ('-a', '--addr'):
            ADDR = arg
        if opt in ('-p', '--port'):
            PORT = int(arg)

    client_info(ADDR, PORT)

    # Создать сокет TCP
    # s.connect((ADDR, PORT))  # Соединиться с сервером

    presence_msg = {
        "action": "presence",
        "time": '',
        "type": '',
        "user": {
            "account_name": "C0deMaver1ck",
            "status": "Yep, I am here!"
        }
    }

    if len(remainder) > 1:
        print('слишком много аргументов')

    if 'send' in remainder:
        presence_msg['type'] = 'send'
        sock = socket(AF_INET, SOCK_STREAM)
        sock.connect((ADDR, PORT))
        send_msg(presence_msg, sock)
        sock.close()
        sender_proc = Process(target=msg_sender, )
        sender_proc.start()
        sender_proc.join()

    elif 'receive' in remainder:
        s = socket(AF_INET, SOCK_STREAM)
        s.connect((ADDR, PORT))
        presence_msg['type'] = 'receive'
        send_msg(presence_msg)
        msg_proc = Process(target=msg_receiver, args=(s,))
        msg_proc.start()
        msg_proc.join()

    else:
        print('По видимому вы ошиблись с аргументом, возможные аргументы \n'
              ' send : режим отправки сообщений \n'
              ' receive : режим приема сообщений \n')
