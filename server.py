# coding: utf8
from socket import *
import time
import json
from multiprocessing import Process, Queue, Manager
import sys
import getopt
import select
import log_config
from log_config import log_debug, log_info

CONNECTIONS_QUANTITY = 5
PORT = 7777
ADDR = '0.0.0.0'


class User:
    def __init__(self, name, mail):
        self.name = name
        self.mail = mail


class Server:
    # test done
    @log_info('старт сервера')
    def start_server(address, port):
        server_info(address, port)
        sok = socket(AF_INET, SOCK_STREAM)  # Создает сокет TCP
        sok.setsockopt(SOL_SOCKET, SO_REUSEPORT, 1)
        sok.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        sok.bind((address, port))  # Присваивает порт 8888
        sok.listen(CONNECTIONS_QUANTITY)  # Переходит в режим ожидания запросов;
        return sok

    # test done
    @log_info('создание очередей')
    def create_pipes():
        mgr = Manager()
        senders_q = mgr.list([])
        receivers_q = mgr.list([])
        m_q = Queue()
        return mgr, senders_q, receivers_q, m_q

    # test done
    @log_debug('Определение клиента')
    def client_qualifier(pipe_receivers, pipe_senders, manager_q, client, msg):
        if msg['type'] == 'send':
            sub = manager_q.list(pipe_senders)
            sub.append(client)
            pipe_senders.append(sub[-1])
        elif msg['type'] == 'receive':
            sub = manager_q.list(pipe_receivers)
            sub.append(client)
            pipe_receivers.append(sub[-1])


class Message:
    def __init__(self, status, action, timestamp, user):
        self.status = status
        self.action = action
        self.timestamp = timestamp
        self.user = user


class MsgSnd:
    pass


class MsgRcv:
    pass


class Chat:
    pass


class ChatController:
    pass




@log_debug('Подготовка параметров запуска сервера')
def start_options_handler():
    options, remainder = getopt.getopt(sys.argv[1:], 'a:p:', ['addr=', 'port='])
    for opt, arg in options:
        print(opt)
        if opt in ('-a', '--addr'):
            global ADDR
            ADDR = arg
        if opt in ('-p', '--port'):
            global PORT
            PORT = int(arg)










# test done
@log_debug('раскодирование сообщения')
def msg_decompress(byte_msg):
    return json.loads(byte_msg.decode('utf-8'))


# test done
@log_debug('кодирование сообщения')
def msg_compress(dict_msg):
    """accept dict and str"""
    return json.dumps(dict_msg).encode('utf-8')


@log_info('прием подключения клиента')
def client_handler(sok):
    client, addr = sok.accept()  # Принять запрос на соединение
    client.setblocking(0)
    return client, addr


def connection_handler(pipe_receivers, pipe_senders, manager_q, sok):
    while True:
        client, addr = client_handler(sok)
        msg = msg_decompress(client.recv(16000))
        print(msg)

        client_qualifier(pipe_receivers, pipe_senders, client, msg, manager_q)

        client.send(msg_compress(time.ctime(time.time())))  # <- По сети должны передаваться байты,

        print("Получен запрос на соединение от %s" % str(addr))


def msg_parser(byte_msg):
    msg_l = byte_msg.decode('utf-8').split('""')
    if len(msg_l) > 1:
        if not msg_l[0]:
            msg_l[0] += '""'
        else:
            msg_l[0] += '"'
        if not msg_l[-1]:
            msg_l[-1] += '""'
        else:
            msg_l[-1] = '"' + msg_l[-1]
    if len(msg_l) > 2:
        for i in range(1, len(msg_l) - 1):
            msg_l[i] = '"' + msg_l[i] + '"'
    return msg_l


def msg_receiver(pipe_clients, pipe_msg):
    while True:
        print('Отправители сообщений', len(pipe_clients))
        r, w, x = select.select(pipe_clients, [], [], 1)
        for i in r:
            try:
                msg = i.recvmsg(16000)
                print('получил: ', msg)
                if msg:
                    if msg[0]:
                        for m in msg_parser(msg[0]):
                            pipe_msg.put(json.loads(m))
            except Exception as e:
                print('msg_receiver', e)
        time.sleep(2)


def msg_sender(pipe_clients, pipe_msg):
    while True:
        # print('меседжи', len(pipe_msg))
        print('Получатели сообщений', len(pipe_clients))
        while not pipe_msg.empty():
            r, w, x = select.select([], pipe_clients, [], 1)
            if w:
                msg = pipe_msg.get()
                print('послал: ', msg)
                # print(pipe_clients)
                if msg:
                    for client in w:
                        client.send(msg_compress(msg))  # <- По сети должны передаваться байты,
        time.sleep(2)


def server_info(addr, port):
    print('Сервер будет запущен со следующими параметрами \n'
          'ip_addr: {}\n'
          'port: {}\n'.format(addr, port))


if __name__ == '__main__':
    start_options_handler()

    s = start_server(ADDR, PORT)

    manager, clients_senders_q, clients_receivers_q, msgs_q = create_pipes()

    conn_proc = Process(target=connection_handler, args=(clients_receivers_q, clients_senders_q, manager, s))
    conn_proc.start()

    msg_proc = Process(target=msg_receiver, args=(clients_senders_q, msgs_q))
    msg_proc.start()

    sender_proc = Process(target=msg_sender, args=(clients_receivers_q, msgs_q))
    sender_proc.start()

    conn_proc.join()
    msg_proc.join()
    sender_proc.join()
